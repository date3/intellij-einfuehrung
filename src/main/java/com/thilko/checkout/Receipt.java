package com.thilko.checkout;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class Receipt {
    private final BigDecimal sum;
    private List<ShoppingCardItem> shoppingCardItems;

    private Receipt(BigDecimal sum, List<ShoppingCardItem> shoppingCardItems) {
        this.sum = sum;
        this.shoppingCardItems = shoppingCardItems;
    }

    public static Receipt create(BigDecimal sum, List<ShoppingCardItem> shoppingCardItems) {
        return new Receipt(sum, shoppingCardItems);
    }

    public BigDecimal getSum() {
        return sum.setScale(2, RoundingMode.HALF_DOWN);
    }

    @Override
    public String toString() {
        return "Receipt{" +
                "sum=" + sum +
                '}';
    }

    public List<ShoppingCardItem> getShoppingCardItems() {
        return shoppingCardItems;
    }
}
