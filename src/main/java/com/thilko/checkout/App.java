package com.thilko.checkout;

public class App {

    public static void main(String[] args) {
        Checkout reweCheckout = new Checkout();
        // TODO: add discount

        reweCheckout.scan(ProductType.BEER, 2);
        reweCheckout.scan(ProductType.APPLES, Weight.asKg("2"));
        reweCheckout.scan(ProductType.MILK, 5);
        // TODO: add new HappyMelonDay discount

        reweCheckout.checkout();
    }
}