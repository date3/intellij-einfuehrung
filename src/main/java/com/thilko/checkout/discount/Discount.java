package com.thilko.checkout.discount;

import com.thilko.checkout.ShoppingCart;

import java.math.BigDecimal;

public interface Discount {

    BigDecimal calculateDiscountFor(ShoppingCart shoppingCart);
}
