package com.thilko.checkout.discount;

import com.thilko.checkout.ShoppingCardItem;
import com.thilko.checkout.ProductType;
import com.thilko.checkout.ShoppingCart;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;

public class TwoEuroForMoreThanFiveMilk implements Discount {
    @Override
    public BigDecimal calculateDiscountFor(ShoppingCart shoppingCart) {
        Map<ProductType, List<ShoppingCardItem>> groupedByTypes = shoppingCart
                .allItems()
                .stream()
                .collect(groupingBy(ShoppingCardItem::getType));

        if (groupedByTypes.getOrDefault(ProductType.MILK, Collections.emptyList()).size() > 5) {
            return BigDecimal.valueOf(2);
        }

        return BigDecimal.ZERO;
    }
}
