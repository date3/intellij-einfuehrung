package com.thilko.checkout.discount;

import com.thilko.checkout.ProductType;
import com.thilko.checkout.ShoppingCart;

import java.math.BigDecimal;

public class PotatoesDiscount implements Discount {
    @Override
    public BigDecimal calculateDiscountFor(ShoppingCart shoppingCart) {
        int potatoesBagCount = 0;

        for (int i = 1; i < shoppingCart.allItems().size(); i++) {
            if (shoppingCart.allItems().get(i).getType().equals(ProductType.POTATOES)) {
                potatoesBagCount++;
            }
        }

        if (potatoesBagCount > 3) {
            return BigDecimal.valueOf(3.5);
        }

        return BigDecimal.ZERO;
    }
}
