package com.thilko.checkout.discount;

import com.thilko.checkout.ShoppingCart;

import java.math.BigDecimal;
import java.util.HashSet;

public class DiscountCalculator {
    private final HashSet<Discount> discounts = new HashSet<>();

    public BigDecimal calculateDiscount(ShoppingCart shoppingCart) {
        BigDecimal amount = BigDecimal.ZERO;
        for (Discount discount : discounts) {
            amount = amount.add(discount.calculateDiscountFor(shoppingCart));
        }

        return amount;
    }

    public void addDiscount(Discount discount) {
        discounts.add(discount);
    }
}
