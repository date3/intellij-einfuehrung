package com.thilko.checkout.printer;

import com.thilko.checkout.Receipt;
import java.text.NumberFormat;

public class ConsolePrinter implements Printer {
    private final NumberFormat instance = NumberFormat.getCurrencyInstance();

    @Override
    public void printReceipt(Receipt receipt) {
        // TODO: print single positions here, use receipt.getShoppingCardItems()

        System.out.printf("Summe: %s%n", instance.format(receipt.getSum()));
    }
}
