package com.thilko.checkout.printer;

import com.thilko.checkout.Receipt;

public interface Printer {

    void printReceipt(Receipt receipt);
}
