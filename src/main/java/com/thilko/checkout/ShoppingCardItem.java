package com.thilko.checkout;

import static java.lang.String.format;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class ShoppingCardItem {
    private final ProductType type;
    private Weight weight;

    private ShoppingCardItem(ProductType type) {
        this.type = type;
    }

    private ShoppingCardItem(ProductType type, Weight weight) {
        this.type = type;
        this.weight = weight;
    }

    public static ShoppingCardItem fromType(ProductType product) {
        if (product.hasWeight()) {
            throw new IllegalStateException(format("You can´t scan %s without weight!", product));
        }

        return new ShoppingCardItem(product);
    }

    public static ShoppingCardItem withWeight(ProductType type, Weight weight) {
        if (!type.hasWeight()) {
            throw new IllegalStateException(format("You can´t scan %s with weight!", type));
        }

        return new ShoppingCardItem(type, weight);
    }

    public BigDecimal calculatePrice() {
        if (type.hasWeight()) {
            return type
                    .getPrice()
                    .multiply(weight.asBigDecimal())
                    .setScale(2, RoundingMode.HALF_UP);
        } else {
            return type
                    .getPrice()
                    .setScale(2, RoundingMode.HALF_UP);
        }
    }

    public ProductType getType() {
        return type;
    }

    @Override
    public String toString() {
        String firstPart = format("%s", type.name());
        if (type.hasWeight()) {
            return format("%s with weight %s", firstPart, weight.asBigDecimal());
        }
        return firstPart;
    }
}
