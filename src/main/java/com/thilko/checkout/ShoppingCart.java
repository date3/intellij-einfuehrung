package com.thilko.checkout;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {
    private List<ShoppingCardItem> shoppingCardItems = new ArrayList<>();

    public void putIn(ShoppingCardItem shoppingCardItem) {
        this.shoppingCardItems.add(shoppingCardItem);
    }

    public List<ShoppingCardItem> allItems() {
        return this.shoppingCardItems;
    }
}
