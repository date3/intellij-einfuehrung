package com.thilko.checkout;

import com.thilko.checkout.discount.Discount;
import com.thilko.checkout.discount.DiscountCalculator;
import com.thilko.checkout.printer.ConsolePrinter;
import com.thilko.checkout.printer.Printer;

import java.math.BigDecimal;

public class Checkout {
    private final Printer printer = new ConsolePrinter();
    private final ShoppingCart shoppingCart = new ShoppingCart();
    private final DiscountCalculator discountCalculator = new DiscountCalculator();

    public Receipt scan(ProductType product) {
        shoppingCart.putIn(ShoppingCardItem.fromType(product));

        // TODO: find and remove code duplicates in this class
        BigDecimal sum = shoppingCart
                .allItems()
                .stream()
                .map(ShoppingCardItem::calculatePrice)
                .reduce(BigDecimal::add).orElse(BigDecimal.ZERO);

        BigDecimal discount = discountCalculator.calculateDiscount(shoppingCart);

        return Receipt.create(sum.subtract(discount), shoppingCart.allItems());
    }

    public Receipt checkout() {
        BigDecimal sum = shoppingCart
                .allItems()
                .stream()
                .map(ShoppingCardItem::calculatePrice)
                .reduce(BigDecimal::add).orElse(BigDecimal.ZERO);

        BigDecimal discount = discountCalculator.calculateDiscount(shoppingCart);

        var receipt = Receipt.create(sum.subtract(discount), shoppingCart.allItems());

        printer.printReceipt(receipt);

        return receipt;
    }

    public Receipt scan(ProductType product, Integer count) {
        for (int i = 0; i < count; i++) {
            scan(product);
        }

        BigDecimal sum = shoppingCart
                .allItems()
                .stream()
                .map(ShoppingCardItem::calculatePrice)
                .reduce(BigDecimal::add).orElse(BigDecimal.ZERO);

        BigDecimal discount = discountCalculator.calculateDiscount(shoppingCart);

        return Receipt.create(sum.subtract(discount), shoppingCart.allItems());
    }

    public BigDecimal scan(ProductType product, Weight weight) {
        ShoppingCardItem shoppingCardItemToAdd = ShoppingCardItem.withWeight(product, weight);
        shoppingCart.putIn(shoppingCardItemToAdd);

        BigDecimal sum = shoppingCart
                .allItems()
                .stream()
                .map(ShoppingCardItem::calculatePrice)
                .reduce(BigDecimal::add).orElse(BigDecimal.ZERO);

        BigDecimal discount = discountCalculator.calculateDiscount(shoppingCart);

        return sum.subtract(discount);
    }

    public void useDiscount(Discount discount) {
        discountCalculator.addDiscount(discount);
    }

}
