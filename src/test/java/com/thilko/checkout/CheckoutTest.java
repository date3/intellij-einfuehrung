package com.thilko.checkout;

import com.thilko.checkout.discount.TwoEuroForMoreThanFiveMilk;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static com.thilko.checkout.ProductType.*;
import static java.lang.String.format;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class CheckoutTest {

    private Checkout checkout;

    @BeforeEach
    public void setUp() {
        checkout = new Checkout();
    }

    @Test
    public void addProduct_noProducts_0euro() {
        assertThat(checkout.checkout().getSum(), is(BigDecimal.ZERO.setScale(2, RoundingMode.HALF_DOWN)));
    }

    @Test
    public void addProduct_3euro50cent_subtotalIs3Euro50cent() {
        Receipt subtotal = checkout.scan(POTATOES);

        assertThat(subtotal.getSum(), is(new BigDecimal("3.50")));
    }

    @Test
    public void addProduct_twoProducts_priceIsAddedCorrectly() {
        checkout.scan(POTATOES);
        Receipt subtotal = checkout.scan(NOODLES);

        assertThat(subtotal.getSum(), is(new BigDecimal("5.50")));
    }

    @Test
    public void addProduct_threeProducts_priceIsAddedCorrectly() {
        checkout.scan(POTATOES);
        checkout.scan(NOODLES);
        Receipt subtotal = checkout.scan(BEER);

        assertThat(subtotal.getSum(), is(new BigDecimal("6.10")));
    }

    @Test
    public void checkout_withProducts_totalSumIsPrintedAndReturned() {
        checkout.scan(POTATOES);
        checkout.scan(NOODLES);
        checkout.scan(BEER);
        checkout.scan(MILK);
        checkout.scan(MILK);

        assertThat(checkout.checkout().getSum(), is(new BigDecimal("8.10")));
    }

    @Test
    public void scan_withCount2_isScannedTwice() {
        Receipt receipt = checkout.scan(BEER, 2);

        assertThat(receipt.getSum(), is(new BigDecimal("1.20")));
    }

    @Test
    public void scan_productWithWeight_priceIsCalculatedCorrectly() {
        BigDecimal subtotal = checkout.scan(ProductType.APPLES, Weight.asKg("3.45"));

        assertThat(subtotal, is(new BigDecimal("12.08")));
    }

    @Test
    public void scan_notAbleToScanBeerWithWeight_exception() {
        Assertions.assertThrows(IllegalStateException.class, () -> {
            checkout.scan(ProductType.BEER, Weight.asKg("3.45"));
        });
    }

    @Test
    public void discountsCanBeApplied_2EuroForMoreThan5milks() {
        checkout.scan(ProductType.MILK, 6);
        checkout.useDiscount(new TwoEuroForMoreThanFiveMilk());

        Receipt receipt = checkout.checkout();
        assertThat(receipt.getSum().intValue(), is(4));
    }
}
