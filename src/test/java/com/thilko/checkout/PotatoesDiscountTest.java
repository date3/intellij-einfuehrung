package com.thilko.checkout;

import com.thilko.checkout.discount.PotatoesDiscount;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class PotatoesDiscountTest {

    @Test
    @Disabled
    public void threeBagsPotatoes_threeFiftyDiscount_totalSumIsSevenEuro() {
        Checkout checkout = new Checkout();
        checkout.useDiscount(new PotatoesDiscount());
        checkout.scan(ProductType.POTATOES, 3);

        assertThat(checkout.checkout().getSum(), is(new BigDecimal("7.00")));
    }
}
